# -*- coding: utf-8 -*-
##############################################################################
#
#    GNU Health: The Free Health and Hospital Information System
#    Copyright (C) 2008-2015 Luis Falcon <falcon@gnu.org>
#    Copyright (C) 2011-2015 GNU Solidario <health@gnusolidario.org>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from datetime import datetime, timedelta
from trytond.model import ModelView, fields
from trytond.wizard import Wizard, StateTransition, StateView, Button,\
    StateAction
from trytond.transaction import Transaction
from trytond.pool import Pool

__all__ = ['PrescriptionBatchMedicament',           
           'RequestPrescriptionBatchStart',
           'RequestPrescriptionBatch']


class PrescriptionBatchMedicament(ModelView):
    'Prescription Batch - Medicament'
    __name__ = 'gnuhealth_prescription_batch_gnuhealth.medicament'
    _table = 'gnuhealth_prescription_line_medicament_wizard'
    
    prescription_batch = fields.Many2One('gnuhealth.patient.presc.batch.start',
                'Request', required=True)
    
    medicament = fields.Many2One('gnuhealth.medicament',
                            'Medicament', required=True)
    
    quantity = fields.Integer('Cantidad',
            help='Cantidad de unidades')
    
    dose_unit = fields.Many2One(
        'gnuhealth.dose.unit', 'Unidad de la dosis')
    
    common_dosage = fields.Many2One(
        'gnuhealth.medication.dosage', 'Frecuencia',
        help=u'Frecuencia común / estandar para la dosis de este medicamento')
    
    dose = fields.Float('Potencia', 
            help=u'Presentación principio activo\nPor ejemplo= 250 mg')
    
    form = fields.Many2One('gnuhealth.drug.form', 'Forma',
                           help='Forma de la droga, como tableta o gel o compromido')    
    
    @fields.depends('common_dosage')
    def on_change_with_quantity(self):
        if self.common_dosage and self.common_dosage.daily_quantity:
                return round(self.common_dosage.daily_quantity*30)


class RequestPrescriptionBatchStart(ModelView):
    'Request Prescription Batch Start'
    __name__ = 'gnuhealth.patient.presc.batch.start'
    
    start_date = fields.DateTime('Fecha de inicio', required=True)
    period = fields.Selection([
        ('one_month', u'1 Mes'),
        ('two_month', u'2 Meses'),
        ('three_month', u'3 Meses'),
        ('four_month', u'4 Meses'),
        ('five_month', u'5 Meses'),
        ('six_month', u'6 Meses'),
        ], u'Period',sort=False, required=True)
    
    patient = fields.Many2One('gnuhealth.patient', 'Patient', required=True)
    
    healthprof = fields.Many2One('gnuhealth.healthprofessional', 'Doctor',
        help="Doctor who Request the lab tests.", required =True, readonly=True)
    
    template_prescription = fields.Many2One('gnuhealth.prescription.order',
        u'Plantilla de prescripción', help=u'Prescripción a copiar')

    medicaments = fields.One2Many(
        'gnuhealth_prescription_batch_gnuhealth.medicament',
        'prescription_batch',u'Líneas',            
        help=u'Líneas de medicamentos')
    
    pregnancy_warning = fields.Boolean(
        'Aviso de embarazo',
        help=u'La droga presenta un riesgo para embarazo o lactancia')
    
    prescription_warning_ack = fields.Boolean(
        u'Prescrición verificada')
    
    add_to_history = fields.Boolean(
        'Hist',
        help=u'Incluir en el historial del paciente?')
    
    main_condition = fields.Many2One(
        'gnuhealth.pathology', u'Condición principal')    
    
    @fields.depends(
        'medicaments',
        'template_prescription')
    def on_change_template_prescription(self):
        lines = self.medicaments
        medicament_lists = []        
        if self.template_prescription:
            for line in self.template_prescription.prescription_line:
                if line.medicament.id not in [y.medicament.id for y in self.medicaments]:
                    batch_line = {}
                    batch_line['medicament'] = line.medicament.id
                    batch_line['common_dosage'] =\
                        line.common_dosage and line.common_dosage.id
                    batch_line['dose'] = line.dose
                    batch_line['dose_unit'] =\
                        line.dose_unit and line.dose_unit.id
                    batch_line['quantity'] = line.quantity
                    batch_line['form'] = line.form.id
                    medicament_lists.append(batch_line)
        self.medicaments += tuple(x for x in medicament_lists)
    
    @staticmethod
    def default_prescription_warning_ack():
        return True

    @staticmethod
    def default_add_to_history():
        return True    

    @staticmethod
    def default_start_date():
        return datetime.now()

    @staticmethod
    def default_patient():
        if Transaction().context.get('active_model') == 'gnuhealth.patient':
            return Transaction().context.get('active_id')
        elif Transaction().context.get('active_model') == 'gnuhealth.prescription.order':
            pool = Pool()
            Prescription = pool.get('gnuhealth.prescription.order')
            prescription = Prescription.search([('id','=',Transaction().context.get('active_id'))])
            patient_id = prescription[0].patient.id
            return patient_id

    @staticmethod
    def default_template_prescription():
        if Transaction().context.get('active_model') == 'gnuhealth.prescription.order':
            return Transaction().context.get('active_id')

    @staticmethod
    def default_healthprof():
        pool = Pool()
        HealthProf= pool.get('gnuhealth.healthprofessional')
        hp = HealthProf.get_health_professional()
        if not hp:
            RequestPrescriptionBatchStart.raise_user_error(
                "No health professional associated to this user !")
        return hp
    
    @staticmethod
    def default_medicaments():
        if Transaction().context.get('active_model') == 'gnuhealth.prescription.order':
            prescription_lines = []
            pool = Pool()
            Prescription = pool.get('gnuhealth.prescription.order')
            prescription = Prescription.search(
                [('id','=',Transaction().context.get('active_id'))]
                )
            if prescription[0].prescription_line:
                for prescr in prescription[0].prescription_line:                
                        line = {}
                        line['medicament'] = prescr.medicament.id
                        line['common_dosage'] =\
                            prescr.common_dosage and prescr.common_dosage.id
                        line['dose_unit'] = prescr.dose_unit and prescr.dose_unit.id
                        line['dose'] = prescr.dose
                        line['form'] = prescr.form.id
                        prescription_lines.append(line)
            return prescription_lines


class RequestPrescriptionBatch(Wizard):
    'Request Prescription Batch'
    __name__ = 'gnuhealth.patient.prescription.batch.request'

    start = StateView('gnuhealth.patient.presc.batch.start',
        'health_prescription_batch.patient_prescription_batch_request_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),            
            Button('Generar e Imprimir', 'request', 'tryton-print', default=True),
            ])
    
    request = StateAction(
        'health_prescription_batch.report_prescription_batch_information')
    
    def fill_data(self):
        PrescriptionOrder = Pool().get('gnuhealth.prescription.order')
        PrescriptionLine = Pool().get('gnuhealth.prescription.line')        
        period = None
        if self.start.period == 'one_month':
            period = 1        
        elif self.start.period == 'two_month':
            period = 2
        elif self.start.period == 'three_month':
            period = 3
        elif self.start.period == 'four_month':
            period = 4
        elif self.start.period == 'five_month':
            period = 5
        elif self.start.period == 'six_month':
            period = 6
        
        request_date = self.start.start_date
        prescription_orders = []
        for prescription_date in [(request_date+timedelta(days=x)) for x in range(0,30*period,30)]:
            prescription_order = {}
            prescription_order['prescription_date'] = prescription_date
            prescription_order['patient'] = self.start.patient.id
            prescription_order['healthprof'] = self.start.healthprof.id
            prescription_order['pregnancy_warning'] = self.start.pregnancy_warning
            prescription_order['prescription_warning_ack'] = self.start.prescription_warning_ack            
            prescription_order = PrescriptionOrder.create([prescription_order])

            prescription_lines = []
            for medicament in self.start.medicaments:
                prescription_line = {}
                prescription_line['name'] = prescription_order[0].id
                prescription_line['medicament'] = medicament.medicament.id
                prescription_line['common_dosage'] = medicament.common_dosage and medicament.common_dosage.id
                prescription_line['quantity'] = medicament.quantity
                prescription_line['dose'] = medicament.dose
                prescription_line['dose_unit'] = medicament.dose_unit
                prescription_line['form'] = medicament.form.id
                prescription_line['add_to_history'] = self.start.add_to_history
                prescription_line['indication'] = None
                prescription_line['start_treatment'] = prescription_date
                prescription_line['end_treatment'] = prescription_date + timedelta(days=30)                
                prescription_lines.append(prescription_line)
            PrescriptionLine.create(prescription_lines)

            for line in prescription_order[0].prescription_line:
                line.add_to_history = self.start.add_to_history
                line.save()
            prescription_order[0].save()
            prescription_orders.append(prescription_order[0])
        PrescriptionOrder.create_prescription(prescription_orders)

        return {
            'prescriptions': [x.id for x in prescription_orders],
            'main_condition': self.start.main_condition and self.start.main_condition.name,
            }

    def do_request(self,action):
        return action, self.fill_data()
    
