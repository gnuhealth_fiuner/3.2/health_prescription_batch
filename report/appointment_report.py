# -*- coding: utf-8 -*-
##############################################################################
#
#    Health Appointment Report
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from datetime import date, datetime
from trytond.report import Report
from trytond.pool import Pool
from trytond.transaction import Transaction
from dateutil.relativedelta import relativedelta

__all__ = ['AppointmentReport']

class AppointmentReport(Report):
    __name__ = 'gnuhealth.appointment.report'
    
    @classmethod
    def get_age_in_that_time(cls, start_date):
        pass
    
    @classmethod
    def get_population_from_app(cls, age1, age2, gender, checked_in, confirmed,speciality,insurance):
        """ Return Total Number of patient on appointments in the system
        segmented by age group and gender, and assurance"""
        pool = Pool()
        Appointment = pool.get('gnuhealth.appointment')
        res = None
        if not insurance:
            res =  len(Appointment.search([
                        ('patient.edad','>=',str(age1)),
                        ('patient.edad','<=',str(age2)),
                        ('patient.name.gender','=',gender),
                        ('speciality','=',speciality),
                        ('state','in',[checked_in,
                           confirmed]),
            ]))            
        elif insurance == 'yes':
            res =  len(Appointment.search([
                        ('patient.edad','>=',str(age1)),
                        ('patient.edad','<=',str(age2)),
                        ('patient.name.gender','=',gender),
                        ('speciality','=',speciality),
                        ('state','in',[checked_in,
                           confirmed]),
                        ('patient.current_insurance','>','0'),
            ]))        
        elif insurance == 'no':
            res =  len(Appointment.search([
                        ('patient.edad','>=',str(age1)),
                        ('patient.edad','<=',str(age2)),
                        ('patient.name.gender','=',gender),
                        ('speciality','=',speciality),
                        ('state','in',[checked_in,
                           confirmed]),
                        ('patient.current_insurance','=',None),
            ]))
        return res
    
    @classmethod
    def get_total_age_group(cls, age1, age2, checked_in, confirmed, insurance):
        """Total grouped by age"""
        pool = Pool()
        Appointment = pool.get('gnuhealth.appointment')
        res = None
        if not insurance:
            res =  len(Appointment.search([
                        ('patient.edad','>=',str(age1)),
                        ('patient.edad','<=',str(age2)),
                        ('speciality','>',0),
                        ('state','in',[checked_in,
                           confirmed]),
            ]))            
        elif insurance == 'yes':
            res =  len(Appointment.search([
                        ('patient.edad','>=',str(age1)),
                        ('patient.edad','<=',str(age2)),
                        ('speciality','>',0),
                        ('state','in',[checked_in,
                           confirmed]),
                        ('patient.current_insurance','>','0'),
            ]))        
        elif insurance == 'no':
            res =  len(Appointment.search([
                        ('patient.edad','>=',str(age1)),
                        ('patient.edad','<=',str(age2)),
                        ('speciality','>',0),
                        ('state','in',[checked_in,
                           confirmed]),
                        ('patient.current_insurance','=',None),
            ]))
        return res

   
    @classmethod
    def get_context(cls, records, data):
        context = super(AppointmentReport, cls).get_context(records, data)
        pool = Pool()
        Appointment = pool.get('gnuhealth.appointment')
        
        start_date = data['start_date']        
        context['start_date'] = data['start_date']
        
        end_date = data['end_date']
        context['end_date'] = data['end_date']

        confirmed = 'confirmed' if data['confirmed'] else ''
        context['confirmed'] = 'confirmed' if data['confirmed'] else ''

        checked_in = 'checked_in' if data['check_in'] else ''
        context['check_in'] = 'checked_in' if data['check_in'] else ''

        appointments = Appointment.search([
                            ('appointment_date','>=',start_date),
                            ('appointment_date','<',end_date),
                            ('state','in',[checked_in,
                                           confirmed]),
                                        ])
    
        specialities = set()
        context['specialities'] = set()
        for appointment in appointments:
            if appointment.speciality:
                specialities.add(appointment.speciality)
                context['specialities'].add(appointment.speciality)
        
        context['speciality_row'] = {}
        for speciality in specialities:
            context['speciality_row'][str(speciality)] = {}
            
            context['speciality_row'][str(speciality)]['f_under_1'] =\
                cls.get_population_from_app(0,1,'f', checked_in, confirmed, speciality, None)
            context['speciality_row'][str(speciality)]['m_under_1'] =\
                cls.get_population_from_app(0,1,'m', checked_in, confirmed, speciality, None)
            
            context['speciality_row'][str(speciality)]['f_1'] =\
                cls.get_population_from_app(1,2,'f', checked_in, confirmed, speciality, None)
            context['speciality_row'][str(speciality)]['m_1'] =\
                cls.get_population_from_app(1,2,'m', checked_in, confirmed, speciality, None)
            
            context['speciality_row'][str(speciality)]['f_2_4'] =\
                cls.get_population_from_app(2,5,'f', checked_in, confirmed, speciality, None)
            context['speciality_row'][str(speciality)]['m_2_4'] =\
                cls.get_population_from_app(2,5,'m', checked_in, confirmed, speciality, None)
            
            context['speciality_row'][str(speciality)]['f_5_9'] =\
                cls.get_population_from_app(5,10,'f', checked_in, confirmed, speciality, None)
            context['speciality_row'][str(speciality)]['m_5_9'] =\
                cls.get_population_from_app(5,10,'m', checked_in, confirmed, speciality, None)
            
            context['speciality_row'][str(speciality)]['f_10_14'] =\
                cls.get_population_from_app(10,15,'f', checked_in, confirmed, speciality, None)
            context['speciality_row'][str(speciality)]['m_10_14'] =\
                cls.get_population_from_app(10,15,'m', checked_in, confirmed, speciality, None)
            
            context['speciality_row'][str(speciality)]['f_15_49'] =\
                cls.get_population_from_app(15,50,'f', checked_in, confirmed, speciality, None)
            context['speciality_row'][str(speciality)]['m_15_49'] =\
                cls.get_population_from_app(15,50,'m', checked_in, confirmed, speciality, None)
            
            context['speciality_row'][str(speciality)]['f_50_beyond'] =\
                cls.get_population_from_app(50,1000,'f', checked_in, confirmed, speciality, None)
            context['speciality_row'][str(speciality)]['m_50_beyond'] =\
                cls.get_population_from_app(50,1000,'m', checked_in, confirmed, speciality, None)
            
            context['speciality_row'][str(speciality)]['total'] =\
                cls.get_population_from_app(0,1000,'m', checked_in, confirmed, speciality, None)
            
            context['speciality_row'][str(speciality)]['total_assured'] =\
                cls.get_population_from_app(0,1000,'m', checked_in, confirmed, speciality, 'yes')
            
            context['speciality_row'][str(speciality)]['total_unassured'] =\
                cls.get_population_from_app(0,1000,'m', checked_in, confirmed, speciality, 'no')
            
        context['total_under_1'] =\
            cls.get_total_age_group(0, 1, checked_in, confirmed, None)
        context['total_1'] =\
            cls.get_total_age_group(1, 2, checked_in, confirmed, None)        
        context['total_2_4'] =\
            cls.get_total_age_group(2, 5, checked_in, confirmed, None)
        context['total_5_9'] =\
            cls.get_total_age_group(5, 10, checked_in, confirmed, None)
        context['total_10_14'] =\
            cls.get_total_age_group(10, 15, checked_in, confirmed, None)
        context['total_15_49'] =\
            cls.get_total_age_group(15, 50, checked_in, confirmed, None)
        context['total_50_beyond'] =\
            cls.get_total_age_group(50, 1000, checked_in, confirmed, None)
        context['total'] =\
            cls.get_total_age_group(0, 1000, checked_in, confirmed, None)        
        context['total_assured'] =\
            cls.get_total_age_group(0, 1000, checked_in, confirmed, 'yes')
        context['total_unassured'] =\
            cls.get_total_age_group(0, 1000, checked_in, confirmed, 'no')        

        return context

