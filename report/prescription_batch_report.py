# -*- coding: utf-8 -*-
##############################################################################
#
#    Health Prescription Batch
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from datetime import date
from trytond.report import Report
from trytond.pool import Pool
from trytond.transaction import Transaction
from dateutil.relativedelta import relativedelta

__all__ = ['PrescriptionBatchReport']

class PrescriptionBatchReport(Report):
    __name__ = 'gnuhealth.prescription.batch.report'
   
    @classmethod
    def get_context(cls, records, data):
        Prescription = Pool().get('gnuhealth.prescription.order')
        
        if 'prescriptions' in data:
            prescriptions = Prescription.search([('id','in',data['prescriptions'])])
        else:
            prescriptions = records
        
        if 'main_condition' in data:
            main_condition = data['main_condition']
        else:
            main_condition = None

        context = super(PrescriptionBatchReport, cls).get_context(records, data)        

        context['prescriptions'] = prescriptions
        
        context['main_condition'] = main_condition

        return context

